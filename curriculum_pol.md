Curriculum Vitae
-------
![](https://virtual.ecaib.org/pluginfile.php/65363/user/icon/adaptable/f1?rev=871651)

Información Personal

Nombre: Pol Espina-Webb Aparicio

Teléfono: 670915362

Correo electronico: [48035162f@iespoblenou.org](48035162f@iespoblenou.org)

Formació Acadèmica
-

Educació Secundaria Obligatòria


Grau Mitja – Sistemes Microinformàtics i Xarxes

## Experiència Laboral ##

Pràctiques a la empresa Media Markt (300h)

## Idiomes ##

Català (Natiu)

Castellà (Natiu)

Anglès (Natiu)

## Competències ##

- Disponibilitat parcial (Matins)
- Actitud proactiva.
- Puntual i segur de mi mateix.
- Responsable i madur.

## Informació Addicional ##

-Administrador de S.O

-Reparació d’equips informàtics

-Instal·lació i configuració d'equips informàtics

-Instal·lació de diferents programes

-Instal·lació i configuració de Router i Switch Bàsic

-Edició de fotografies i videos.

-Creació d'instrumentals de diversos generes.






